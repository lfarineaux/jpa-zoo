package dao;

import java.util.Collection;

public interface IDao<T> {
	public T add(T entity);

	public T find(Object id);

	public T update(T entity);

	public Collection<T> findNamedQuery(String query);
	
	public void remove(int id);
}
