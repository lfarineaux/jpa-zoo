package service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import dao.AlimentDao;
import dto.AlimentDto;
import entity.Aliment;

public class ServiceAliment {
	AlimentDao alimentDao = new AlimentDao();
	AlimentDto alimentDto = new AlimentDto();


	private EntityManagerFactory factory = null;

	public ServiceAliment() {
		this.factory = Persistence.createEntityManagerFactory("zoo");
	}

	//
	//	public void close() {
	//		if (factory != null) {
	//			factory.close();
	//		}
	//	}

	// Creer un EM et ouvrir une transaction
	private EntityManager newEntityManager() {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		return (em);
	}
	// Fermer un EM et defaire la transaction si necessaire
	private void closeEntityManager(EntityManager em) {
		if (em != null) {
			if (em.isOpen()) {
				EntityTransaction t = em.getTransaction();
				if (t.isActive()) {
					try {
						t.rollback();
					} catch (PersistenceException e) {
					}
				}
				em.close();
			}
		}
	}


	public void creerAliment(String in) {
		try {
			Aliment a = new Aliment();
			a.setNomAliment(in);
			// insertion de l aliment dans la dao
			alimentDao.add(a);
		} catch (Exception e) {
			System.out.println(e);
		}
	}



	//	public List<Aliment> findAll() {
	//		EntityManager em = null;
	//		try {
	//			em = newEntityManager();
	//			String query = "SELECT p FROM Aliment p";
	//			TypedQuery<Aliment> q = em.createQuery(query, Aliment.class);
	//			return q.getResultList();
	//		} finally {
	//			closeEntityManager(em);
	//		}
	//	}

	//	public Object findIdByAlim(String nom) {
	//		EntityManager em = null;
	//		try {
	//			em = newEntityManager();
	//			TypedQuery<Aliment> q = em.createNamedQuery("findIdByAlim", Aliment.class);
	//			q.setParameter("NameParam", nom);
	//			return q.getSingleResult();
	//		} finally {
	//			closeEntityManager(em);
	//		}
	//	}


	public List<Aliment>  findAll() {
		List <Aliment> list = new ArrayList<>();
		for (Aliment alim : alimentDao.findNamedQuery("findAll")) {
			list.add(alim);
		}

		return list;

	}


	public Collection<AlimentDto> afficherAll() {
		List<AlimentDto> listalimentdto = new ArrayList<>();
		for (Aliment alim : alimentDao.findNamedQuery("findAll")) {
			alimentDto.setNomAliment(alim.getNomAliment());
			listalimentdto.add(alimentDto);
		}
		return listalimentdto;
	}

	//	public static void majAliment(String res3, String resnew) {
	//		try {
	//			AlimentDao aldao = new AlimentDao();
	//			Aliment re = aldao.findAllByNamedQuery("AlFindAll").stream().filter(pe -> pe.getNomAliment().equals(res3))
	//					.findAny().get();
	//
	//			re.setNomAliment(resnew);
	//			alimentDao.update(re);
	//		} catch (Exception e) {
	//			System.out.println(e);
	//			// TODO: handle exception
	//		}
	//	}

	public void majAliment(String nom, int id) {
		Aliment a = alimentDao.find(id);
		a.setNomAliment(nom);
		alimentDao.update(a);
	}

	public  void suppAliment(int id) {
		try {
			alimentDao.remove(id);
			//			Aldao.findAllByNamedQuery("delete from aliment a where nom_aliment = '"+res5+"'");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
